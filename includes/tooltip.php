<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class TooltipData extends Cache {

	protected $db;
	protected $dbh;

	function __construct($db) {
		$this->db = $db;
		$this->dbh = $db->dbh;
	}

	/**
	 * Get item. At least one parameter must be provided (GUID or entry)
	 * @param integer guid (optional) Item GUID.
	 * @param integer entry (optional) Item entry.
	 */
	public function get_item($guid=0,$entry=0) {
		global $shared;

		if ($out = $this->get_cache(array('tooltip_item',($guid ? '_guid_'.$guid : '_entry_'.$entry)),($guid ? TOOLTIP_EXPIRE : ITEM_EXPIRE))) {
			return $out;
		}

		$item = new Item($this->db);

		if ($guid != 0) {
			$item->get_by_guid($guid);
		} else if ($entry != 0) {
			$item->get_by_entry($entry);
		} else {
			return false;
		}


		if ($o = $item->get_item()) {

			$out = array();

			$html = '<div style="line-height: 17px; font-size: 12px; color: #fff;">';
			$html .= '<div style="font-size: 14px; line-height: 19px; color:'.$shared['ItemColors'][$o['quality']].'">'.$o['name'].'</div>';
			$html .= '<div style="color: #ffd100">Item Level '.$o['itemLevel'].'</div>';
			$html .= '<div>'.$shared['ItemBondingType'][$o['bonding']].'</div>';
			if ($o['itemClass'] == 1) { // bag
				$html .= '<div>'.$o['ContainerSlots'].' Slot '.$shared['InventoryType'][$o['inventoryType']].'</div>';
			} else { // dont show inventory type and subclass on certain items (like bags)
				$html .= '<div style="float: left">'.$shared['InventoryType'][$o['inventoryType']].'</div>';
				$html .= '<div style="float: right">'.$shared['ItemClass'][$o['itemClass']][$o['itemSubClass']].'</div>';
				$html .= '<div style="clear: both"></div>';
			}
			if ($o['itemClass'] == ITEM_CLASS_ARMOR) {
				// armor calculation - 7.14 looks like contant for legs. Need to rework!!!
				if ($o['itemSubClass'] >=1 && $o['itemSubClass'] <=4) {
					$html .= '<div>'.$item->get_armor().' Armor</div>';

				}
			} else if ($o['itemClass'] == ITEM_CLASS_WEAPON) {
				$damage = $item->get_damage();
				$html .= '<div style="float: left">'.$damage['min'].' - '.$damage['max'].' Damage</div>';
				$html .= '<div style="float: right">Speed '.sprintf("%.2f",$item->speed).'</div>';
				$html .= '<div style="clear: both"></div>';
				$html .= '<div>('.$damage['dps'].' damage per second)</div>';
			}


			for ($i=0;$i<=9;++$i) {		// base stat (white)
				if ($o['statType_'.$i] != -1 && $o['statType_'.$i] > 0 && ($o['statType_'.$i] >= 3 && $o['statType_'.$i] <=7)) {
					$html .= '<div>+'.$o['statValue_'.$i].' '.$shared['ItemModType'][$o['statType_'.$i]].'</div>';
				}
			}


			if ($guid != 0) {
				$e = $item->get_enchant(0);	// permanent enchantments
				if ($e[0]) {
					$html .= '<div style="color: #1eff00">'.$e[3].'</div>';
				}
			}

			for ($i=0;$i<=9;++$i) {		// green stat (on equip)
				if ($o['statType_'.$i] != -1 && $o['statType_'.$i] > 0 && ($o['statType_'.$i] < 3 || $o['statType_'.$i] > 7)) {
					$html .= '<div style="color:#1eff00">Equip: +'.$o['statValue_'.$i].' '.$shared['ItemModType'][$o['statType_'.$i]].'</div>';
				}
			}

			for ($i=1;$i<=5;++$i) {		// green stat (spell triggers)
				if ($o['spell_'.$i] > 0) {
					$spell = new Spell($this->db,$o['spell_'.$i]);
					$html .= '<div style="color:#1eff00">Equip: <a style="color:#1eff00" href="spell.html#'.$o['spell_'.$i].'">'.$spell->get_description().'</a></div>';
				}
			}

			
			if ($guid != 0) {
				for ($i=10;$i<=14;++$i) {
					$e = $item->get_enchant($i);
					if ($e[0]) {
						$prop = new SpellItemEnchantment($this->db,$e[0]);
						$html .= '<div style="color: #1eff00">'.$e[3].'</div>';
					}
				}
			}

			
			// gem sockets
			if ($o['socket_bonus']) {
				$html .= '<br />';
				for ($i=1;$i<=3;++$i) {
					if ($o['socket_'.$i] != 0) {
						$html .= '<div><img src="http://new.wowarmory.sk/images/socket-'.$shared['SocketColor'][$o['socket_'.$i]].'.gif"> ';
						// inserted gems only for items by guid
						if ($guid != 0) {
							$e = $item->get_enchant($i+1);
							$gem = new SpellItemEnchantment($this->db,$e[0]);

							$color = $gem->get_gem_color();
							if ($color) {
								$html .= ' <img src="http://new.wowarmory.sk/images/inv_jewelcrafting_'.$color.'.png"> ';
							}
							$html .= ($e[0] != 0 ? $e[3] : $shared['SocketColor'][$o['socket_'.$i]].' socket') .'</div>';
						} else {
							$html .= $shared['SocketColor'][$o['socket_'.$i]].' socket</div>';

						}
					}
				}

				// prismatic enchants only for items by guid
				if ($guid != 0) {
					$e = $item->get_enchant(6);	// buckle (PRISMATIC_ENCHANTMENT_SLOT)
					if ($e[0]) {
						$e2 = $item->get_enchant(4);
						$gem = new SpellItemEnchantment($this->db,$e2[0]);
						$html .= '<div style="color:#1eff00"><img src="http://new.wowarmory.sk/images/socket-prismatic.gif"> ';
						$color = $gem->get_gem_color();
						if ($color) {
							$html .= ' <img src="http://new.wowarmory.sk/images/inv_jewelcrafting_'.$color.'.png"> ';
						}
						$html .= $e2[3].'</div>';
					}
				}

				// gem bonuses
				$e = $item->get_enchant(5);
				$socket_bonus = new SpellItemEnchantment($this->db,$o['socket_bonus']);
				$html .= '<div style="color: '.($e[0] != 0 ? '#1eff00' : '#9d9d9d' ).'">Socket bonus:'.$socket_bonus->get_name().'</div>';
			}


			$html .= '<br />';
			$max_durability = $item->get_max_durability();
			if ($max_durability > 0) {
				if ($guid != 0) {
					$html .= '<div>Durability '.$o['durability'].'/'.$max_durability.'</div>';
				} else if ($entry != 0) {
					$html .= '<div>Durability '.$max_durability.'</div>';
				}
			}



			if ($o['allowableClass'] > 0 && $o['allowableClass'] < 1535) {
				$classes = array();
				for ($i=1;$i<sizeof($shared['Classes']);++$i) {
					if ($o['allowableClass'] & (1<<($i-1))) {
						$classes[] = $shared['Classes'][$i];
					}
				}
				$html .= '<div>Classes: '.implode(', ',$classes).'</div>';
			}
			if ($o['allowableRace'] > 0 && $o['allowableRace'] != 2147483647) {
				$races = array();
				for ($i=1;$i<sizeof($shared['Races']);++$i) {
					if ($o['allowableRace'] & (1<<($i-1))) {
						$races[] = $shared['Races'][$i];
					}
				}
				$html .= '<div>Races: '.implode(', ',$races).'</div>';
			}
			if ($o['requiredLevel'] > 0) {
				$html .= '<div>Requires Level '.$o['requiredLevel'].'</div>';
			}




/*
TODO
    int32      AllowableClass;                               // 10      7
    int32      AllowableRace;                                // 11      8
    int32      RequiredLevel;                                // 13      10
    uint32     RequiredSkill;                                // 14      11
    uint32     RequiredSkillRank;                            // 15      12
    uint32     RequiredSpell;                                // 16      13
    uint32     RequiredHonorRank;                            // 17      14
    uint32     RequiredCityRank;                             // 18      15
    uint32     RequiredReputationFaction;                    // 19      16
    uint32     RequiredReputationRank;                       // 20      17
*/


			if ($o['item_set'] > 0) {
				$html .= '<br />';
				$item_set = new ItemSet($this->db,$o['owner_guid']);
				$item_set->lookup_set($o['item_set']);
				$html .= '<div style="color:#ffd100">'.$item_set->get_set_name().' ('.$item_set->get_equipped_pieces().'/'.$item_set->get_max_pieces().')</div>';
				foreach ($item_set->get_pieces() as $itempiece) {
					$html .= '<div style="padding-left: 9px;"><a style="color: '.(isset($itempiece->is_equipped_instead) ? '#FCFFB0' : '#9d9d9d').'" href="item.html#'.$itempiece->entry.'">'.$itempiece->name.'</a></div>';
				}

				$html .= '<br />';

				foreach ($item_set->get_set_bonuses() as $b) {
					$spell = $b[1]->get_spell();
					$html .= '<div style="color: '. ($item_set->get_equipped_pieces() >= $b[0] ? '#1eff00' : '#9d9d9d').'">('.$b[0].') Set: <a style="color: '. ($item_set->get_equipped_pieces() >= $b[0] ? '#1eff00' : '#9d9d9d').'" href="spell.html#'.$spell['id'].'">'.$spell['description'].'</a></div>';

				}
			}

			if ($o['SellPrice'] > 0) {
				$html .= '<div>Sell Price: '.$this->_calculate_money($o['SellPrice']).'</div>';
			}


			$itemscore = $item->score;
			$html .= '<div>ItemScore: '.$itemscore.'</div>';

			
			$html .= '<div style="padding-top: 5px; font-size: 10px; color: #9d9d9d; float: right">from CatArmory</div>';
                        $html .= '<div style="clear: both"></div>';

			$html .= '</div>';

			$out = array(
				'icon' => $o['icon'],
				'html' => $html
			);
		} else {
			$out = array(
				'icon' => 'unknown',
				'html' => 'item not found'
			);
		}
		$this->store_cache(array('tooltip',($guid ? '_guid_'.$guid : '_entry_'.$entry)),$out);

		return $out;
	}

        /**
	 * Get spell.
	 * @param integer entry Spell DBC entry.
	 */
	public function get_spell($entry=0) {
		global $shared;

		if ($out = $this->get_cache(array('tooltip_spell',$entry),TOOLTIP_EXPIRE)) {
			return $out;
		}

		if ($entry != 0) {
			// lets get it trough Spell() class
			$spell = new Spell($this->db,$entry);

			$s = $spell->get_spell();

			$html = '<div style="font-size: 14px;">'.$s['name'].'</div>';
			$html .= '<div style="font-size: 12px;">'.$s['rank'].'</div>';
			$html .= '<div style="color: #ffd100; font-size: 12px;">'.$s['description'].'</div>';

			$html .= '<div style="padding-top: 5px; font-size: 10px; color: #9d9d9d; float: right">from CatArmory</div>';
			$html .= '<div style="clear: both"></div>';


			$out = array(
				'icon' => $s['icon'],
				'html' => $html
			);
		} else {
			$out = array(
				'icon' => 'unknown',
				'html' => 'spell not found'
			);
		}

			
		$this->store_cache(array('tooltip_spell',$entry),$out);

                return $out;
	}


        /**
	 * Get character. At least one parameter must be provided (GUID or name)
	 * @param integer guid (optional) Characeter GUID.
	 * @param string name (optional) Character name.
	 */
	public function get_character($guid=0,$name='') {
		global $shared;

		if ($out = $this->get_cache(array('tooltip_character',$guid),TOOLTIP_EXPIRE)) {
			return $out;
		}

		if ($guid != 0) {
			// lets get it trough Character() class
			$char = new Character($this->db,$guid);

			$c = $char->get_char();

			$html = '<div style="font-size: 14px; float: left; color: #ffd100">'.$c['name'].'</div>';
			$html .= '<div style="font-size: 13px; float: right">Level '.$c['level'].' '.$shared['Races'][$c['race']].' <span style="color: '.$shared['classColors'][$c['class']].'">'.$shared['Classes'][$c['class']].'</span></div>';
			$html .= '<div style="clear: both"></div>';
			if ($c['guildid'] > 0) {
				$html .= '<div style="padding-top: 5px"><span style="color: #9d9d9d">&lt;'.$c['guildRank'].'&gt;</span> of '.$c['guildName'].'</div>';
			}

			$inventory = $char->get_char_items();
			$html .= '<div style="padding-top: 5px">Gearscore: <span style="color: #e6cc80">'.$inventory['gearscore'].'</span></div>';

			$html .= '<div style="padding-top: 5px">Specializations:<ul style="padding:0; margin:0 0 0 25px;">';
			$talents = $char->get_char_talents();
			$tab = array(array(),array());
			foreach ($talents['talents'] as $t) {
				if (array_key_exists($t['grp'],$tab[$t['spec']])) {
					$tab[$t['spec']][$t['grp']] += $t['points'];
				} else {
					$tab[$t['spec']][$t['grp']] = $t['points'];
				}
			}

			$tabs = array();
			foreach ($talents['tabs'] as $t) {
				$tabs[$t['tree_index']] = $t;
			}

			for ($i=0;$i<=1;++$i) {
				$max = 0;
				$points = array();
				$name = '';
				foreach ($tabs as $t) {
					$sum = array_key_exists($t['tree_id'],$tab[$i]) ? $tab[$i][$t['tree_id']] : 0;
					$points[] = $sum;
					if ($sum > $max) {
						$name = $t['tree_name'];
						$max = $sum;
					}
				}

				$html .= '<li><span style="color: #e6cc80">'.($i == 0 ? 'Primary' : 'Secondary').'</span>: '.implode(' / ',$points);
				if ($name) {
					$html .= ' ('.$name.')';
				}
				$html .= '</li>';

			}
			$html .= '</div>';


			$skills = $char->get_char_skills();
			$tradeskills = array();
			foreach ($skills as $s) {
				if ($s['grp'] == 11) {
					$tradeskills[] = $s;
				}
			}
			if (sizeof($tradeskills) > 0) {
				$html .= '<div style="padding-top: 5px">Tradeskills:<ul style="padding:0; margin:0 0 0 25px;">';
				foreach ($tradeskills as $s) {
					$html .= '<li><span style="color: #e6cc80">'.$s['name'].'</span> ('.$s['value'].' / '.$s['max'].')</li>';
				}
				$html .= '</ul></div>';
			}


			$html .= '<div style="padding-top: 5px; font-size: 10px; color: #9d9d9d; float: right">from CatArmory</div>';
			$html .= '<div style="clear: both"></div>';



			$out = array(
				'html' => $html
			);
		} else {
			$out = array(
				'html' => 'character not found'
			);
		}

		$this->store_cache(array('tooltip_character',$guid),$out);

                return $out;
	}


        /**
	 * Get spawn.
	 * @param integer guid Spawn GUID.
	 */
	public function get_spawn($guid=0) {
                global $shared;

                if ($out = $this->get_cache(array('tooltip_spawn',$guid),NPC_EXPIRE)) {
                        return $out;
                }

                if ($guid != 0) {
			$spawn = new Spawn($this->db,$guid);
			$s = $spawn->get_spawn();

			$html = '<div>Respawn: '.$s['spawntimesecs'].'s</div>';

                        $out = array(
                                'html' => $html
                        );
                } else {
                        $out = array(
                                'html' => 'spawn not found'
                        );
                }


                $this->store_cache(array('tooltip_spawn',$guid),$out);

                return $out;

	}


        /**
         * Get arenateam. At least one parameter must be provided (GUID or entry)
         * @param integer guid (optional) Arenateam GUID.
         * @param string name (optional) Arenateam name.
         */
	public function get_arenateam($guid=0,$name='') {
                global $shared;


		$team = new Arenateam($this->db);
                if ($guid != 0) {
	                if ($out = $this->get_cache(array('tooltip_arenateam',$guid),TOOLTIP_EXPIRE)) {
        	                return $out;
                	}
			$team->get_by_guid($guid);
		} else if ($name != '') {
	                if ($out = $this->get_cache(array('tooltip_arenateam',$name),TOOLTIP_EXPIRE)) {
        	                return $out;
	                }
			$team->get_by_name($name);
		}

		if ($t = $team->get_team()) {

			$html = '<div style="font-size: 14px; float: left; color: #ffd100">'.$t['arenateamName'].'</div>';
			$html .= '<div style="font-size: 13px; float: right">'.$t['type'].'v'.$t['type'].'</div>';
			$html .= '<div style="clear: both"></div>';

			foreach ($t['members'] as $member) {
				$char = new Character($this->db,$member['guid']);
				$c = $char->get_char();

				$html .= '<div style="padding-top: 5px;"></div>';

				$html .= '<div style="font-size: 12px; float: left; font-weight: bold">'.$c['name'].'</div>';
				$html .= '<div style="font-size: 12px; float: right">Level '.$c['level'].' '.$shared['Races'][$c['race']].' <span style="color: '.$shared['classColors'][$c['class']].'">'.$shared['Classes'][$c['class']].'</span></div>';
				$html .= '<div style="clear: both"></div>';

				$html .= '<div style="padding-left: 10px;">';
				$inventory = $char->get_char_items();
				$html .= '<div style="padding-top: 5px;"><span style="color: #9d9d9d">Gearscore:</span> <span style="color: #e6cc80">'.$inventory['gearscore'].'</span></div>';

				$html .= '<div style="padding-top: 5px;"><span style="color: #9d9d9d">Specializations:</span><ul style="padding:0; margin:0 0 0 25px;">';
				$talents = $char->get_char_talents();
				$tab = array(array(),array());
				foreach ($talents['talents'] as $t) {
					if (array_key_exists($t['grp'],$tab[$t['spec']])) {
						$tab[$t['spec']][$t['grp']] += $t['points'];
					} else {
						$tab[$t['spec']][$t['grp']] = $t['points'];
					}
				}

				$tabs = array();
				foreach ($talents['tabs'] as $t) {
					$tabs[$t['tree_index']] = $t;
				}

				for ($i=0;$i<=1;++$i) {
					$max = 0;
					$points = array();
					$name = '';
					foreach ($tabs as $t) {
						$sum = array_key_exists($t['tree_id'],$tab[$i]) ? $tab[$i][$t['tree_id']] : 0;
						$points[] = $sum;
						if ($sum > $max) {
							$name = $t['tree_name'];
							$max = $sum;
						}
					}

					$html .= '<li><span style="color: #e6cc80">'.($i == 0 ? 'Primary' : 'Secondary').'</span>: '.implode(' / ',$points);
					if ($name) {
						$html .= ' ('.$name.')';
					}
					$html .= '</li>';

				}
				$html .= '</div>';
				$html .= '</div>';
			}


                        $out = array(
                                'html' => $html
                        );
                } else {
                        $out = array(
                                'html' => 'team not found'
                        );
                }
                if ($guid != 0) {
			$this->store_cache(array('tooltip_arenateam',$guid),$out);
                } else if ($name != '') {
			$this->store_cache(array('tooltip_arenateam',$name),$out);
		}

                return $out;
	}


        /**
	 * Get guild.
	 * @param integer guid Guild GUID.
	 */
	public function get_guild($guid=0,$name='') {
                global $shared;

		$guild = new Guild($this->db);
                if ($guid != 0) {
	                if ($out = $this->get_cache(array('tooltip_guild',$guid),TOOLTIP_EXPIRE)) {
        	                return $out;
                	}
			$guild->get_by_guid($guid);
		} else if ($name != '') {
	                if ($out = $this->get_cache(array('tooltip_guild',$name),TOOLTIP_EXPIRE)) {
        	                return $out;
	                }
			$guild->get_by_name($name);
		}

		if ($g = $guild->get_guild(TRUE)) {	// request with noauth

			$members = $guild->get_members(TRUE);	// request with noauth

			$faction = (1<<($g['leaderRace']-1) & $shared['racemaskAlliance'] ? 'alliance' : 'horde');

			$html = '<div style="font-size: 14px; float: left; color: #ffd100"><img src="http://new.wowarmory.sk/images/'.$faction.'.png"> '.$g['guildName'].'</div>';
			$html .= '<div style="font-size: 13px; float: right">'.sizeof($members).' members</div>';
			$html .= '<div style="clear: both"></div>';

			$html .= '<div style="padding-top: 5px">Level: <span style="color: #e6cc80">'.$g['guildLevel'].'</span> ('.intval(($g['experience']/$g['next_level_xp'])*100).'% of '.human_size($g['next_level_xp']).')</div>';

			if (array_key_exists('listed',$g['recruitment']) && $g['recruitment']['listed'] == 1) {
				$html .= '<div style="padding-top: 5px">Recruitment</div>';
                                $html .= '<div style="padding-left: 10px;">';
				$html .= '<div style="size:10px; color: #9d9d9d; padding-bottom: 5px;">'.$g['recruitment']['comment'].'</div>';

				if ($g['recruitment']['interests'] > 0) {
					$interests = array();
					foreach ($shared['guildRecruitmentInterests'] as $k => $v)
						if ($k & $g['recruitment']['interests'])
							$interests[] = $v;
					$html .= '<div>Interests: '.implode(', ',$interests).'</div>';
				}

				if ($g['recruitment']['availability'] > 0) {
					$availability = array();
					foreach ($shared['guildRecruitmentAvailability'] as $k => $v)
						if ($k & $g['recruitment']['availability'])
							$availability[] = $v;
					$html .= '<div>Availability: '.implode(', ',$availability).'</div>';
				}

				if ($g['recruitment']['classRoles'] > 0) {
					$roles = array();
					foreach ($shared['guildRecruitmentRoles'] as $k => $v)
						if ($k & $g['recruitment']['classRoles'])
							$roles[] = $v;
					$html .= '<div>Roles: '.implode(', ',$roles).'</div>';
				}

				if ($g['recruitment']['level'] > 0) {
					$levels = array();
					foreach ($shared['guildRecruitmentLevels'] as $k => $v)
						if ($k & $g['recruitment']['level'])
							$levels[] = $v;
					$html .= '<div>Levels: '.implode(', ',$levels).'</div>';
				}




#				$html .= '<div style="padding-top: 5px">Recruitment '.print_r($g['recruitment'],1).'</div>';
				$html .= '</div>';

			}


			$out = array(
				'html' => $html
			);
		} else {
			$out = array(
				'html' => 'guild not found'
			);
		}

		if ($guid != 0) {
#			$this->store_cache(array('tooltip_guild',$guid),$out);
		} else if ($name != '') {
#			$this->store_cache(array('tooltip_guild',$name),$out);
		}

                return $out;

	}


	/**
	 * Convert integer money to html representation with images
	 * @param integer int representation of money
	 * @return string html representation of money
	 */
	private function _calculate_money($int) {
		$gold = intval($int / 10000);
		$tmp = $int % 10000;
		$silver = intval($tmp / 100);
		$copper = $tmp % 100;
		return ($gold ? $gold.'<img src="http://new.wowarmory.sk/images/g.gif"> ' : '').(($silver || $int < 100) ? $silver.'<img src="http://new.wowarmory.sk/images/s.gif"> ' : '').($copper ? $copper.'<img src="http://new.wowarmory.sk/images/c.gif">' : '');
	}
	
}

