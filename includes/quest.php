<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class Quest extends Cache {

	protected $_quest;
	protected $db;

	/**
	 * @param PDO database handler
	 * @param integer id of quest
	 */
	function __construct($db,$id) {
		$this->db = $db;
		$this->dbh = $db->dbh;

		// search for cached data. Set variable and stop processing when found.
		if ($this->_quest = $this->get_cache(array('quest',$id),QUEST_EXPIRE)) {
			return;
		}
		
		
		$get_quest = $this->db->query('
			SELECT qt.`Id`,qt.`Title`,qt.`Objectives`,qt.`Details`,qt.`Method`,qt.`Level`,qt.`MinLevel`,qt.`RequiredRaces`,qt.`ZoneOrSort`,qt.`Type`,qt.`PrevQuestId`,qt.`NextQuestId`,qt.`ExclusiveGroup`,qt.`NextQuestIdChain`,qt.`RequiredNpcOrGo1`,qt.`RequiredNpcOrGo2`,qt.`RequiredNpcOrGo3`,qt.`RequiredNpcOrGo4`,qt.`RequiredNpcOrGoCount1`,qt.`RequiredNpcOrGoCount2`,qt.`RequiredNpcOrGoCount3`,qt.`RequiredNpcOrGoCount4`,qt.`RequiredSourceItemId1`,qt.`RequiredSourceItemId2`,qt.`RequiredSourceItemId3`,qt.`RequiredSourceItemId4`,qt.`RequiredSourceItemCount1`,qt.`RequiredSourceItemCount2`,qt.`RequiredSourceItemCount3`,qt.`RequiredSourceItemCount4`,qt.`RequiredItemId1`,qt.`RequiredItemId2`,qt.`RequiredItemId3`,qt.`RequiredItemId4`,qt.`RequiredItemId5`,qt.`RequiredItemId6`,qt.`RequiredItemCount1`,qt.`RequiredItemCount2`,qt.`RequiredItemCount3`,qt.`RequiredItemCount4`,qt.`RequiredItemCount5`,qt.`RequiredItemCount6`,qt.`RewardXpId`,qt.`RewardOrRequiredMoney`,qt.`RewardItemId1`,qt.`RewardItemId2`,qt.`RewardItemId3`,qt.`RewardItemId4`,qt.`RewardItemCount1`,qt.`RewardItemCount2`,qt.`RewardItemCount3`,qt.`RewardItemCount4`,qt.`RewardChoiceItemId1`,qt.`RewardChoiceItemId2`,qt.`RewardChoiceItemId3`,qt.`RewardChoiceItemId4`,qt.`RewardChoiceItemId5`,qt.`RewardChoiceItemId6`,qt.`RewardChoiceItemCount1`,qt.`RewardChoiceItemCount2`,qt.`RewardChoiceItemCount3`,qt.`RewardChoiceItemCount4`,qt.`RewardChoiceItemCount5`,qt.`RewardChoiceItemCount6`,qt.`RewardFactionId1`,qt.`RewardFactionId2`,qt.`RewardFactionId3`,qt.`RewardFactionId4`,qt.`RewardFactionId5`,qt.`RewardFactionValueId1`,qt.`RewardFactionValueId2`,qt.`RewardFactionValueId3`,qt.`RewardFactionValueId4`,qt.`RewardFactionValueId5`
			FROM `'.$this->db->worlddb.'`.`quest_template` AS qt
			WHERE qt.`Id` = ?',
			array($id)
		);

		if ($get_quest->rowCount() == 1) {
			$q = $get_quest->fetch(PDO::FETCH_ASSOC);
			
			
			$get_quests_xp = $this->db->query('
				SELECT `col_1`,`col_2`,`col_3`,`col_4`,`col_5`,`col_6`,`col_7`,`col_8`,`col_9`,`col_10`
				FROM `dbc_questxp`
				WHERE `col_0`=?',
				array($q['MinLevel'])
			);

			$xp_map = $get_quests_xp->fetch(PDO::FETCH_ASSOC);

			// minlevel is used as reference. In game, there is diffFactor of QuestLevel and PlayerLevel
			// trinity core code below from QuestDef.cpp
			// uint32 Quest::XPValue(Player* player) const

			$diff_factor = 2 * ($q['Level'] - $q['MinLevel']) + 20;
			if ($diff_factor < 1)
				$diff_factor = 1;
			else if ($diff_factor > 10)
				$diff_factor = 10;

			$xp = $diff_factor * $xp_map['col_'.($q['RewardXpId']+1)] / 10;

			if ($xp == 0)
				$xp = 0;
			else if ($xp <= 100)
				$xp = 5 * (($xp + 2) / 5);
			else if ($xp <= 500)
				$xp = 10 * (($xp + 5) / 10);
			else if ($xp <= 1000)
				$xp = 25 * (($xp + 12) / 25);
			else
				$xp = 50 * (($xp + 25) / 50);
			// end of trinity code


			$required_npcs = array();
			$required_objects = array();
			$required_items = array();

			$reward_items = array();
			$choose_items = array();

			$gain_reputations = array();
			
			$prequests = array();
			$nextquests = array();


			
			for ($i=1;$i<=4;++$i) {
				if ($q['RequiredNpcOrGo'.$i] > 0) {	// npc
					// TODO
				} else if ($q['RequiredNpcOrGo'.$i] < 0) {	// gameobject
					// TODO
				}
			}

			for ($i=1;$i<=6;++$i) {
				if ($q['RequiredItemId'.$i] > 0) {
					$item = new Item($this->db);
					$item->get_by_entry($q['RequiredItemId'.$i]);
					$req_item = $item->get_item();
					$req_item['count'] = $q['RequiredItemCount'.$i];
					$required_items[] = $req_item;
				}
			}


			// lookup all items in rewards
			for ($i=1;$i<=4;++$i) {
				if ($q['RewardItemId'.$i] > 0) {
					$item = new Item($this->db);
					$item->get_by_entry($q['RewardItemId'.$i]);
					$rew_item = $item->get_item();
					$rew_item['count'] = $q['RewardItemCount'.$i];
					$reward_items[] = $rew_item;
				}
			}
			// lookup all choosable items in rewards
			for ($i=1;$i<=6;++$i) {
				if ($q['RewardChoiceItemId'.$i] > 0) {
					$item = new Item($this->db);
					$item->get_by_entry($q['RewardChoiceItemId'.$i]);
					$rew_item = $item->get_item();
					$rew_item['count'] = $q['RewardChoiceItemCount'.$i];
					$choose_items[] = $rew_item;
				}
			}

			// lookup reputation
			for ($i=1;$i<=5;++$i) {
				if ($q['RewardFactionId'.$i] > 0) {
					// instead of 5 joins - using Faction() class with precached content
					$faction = new Faction($this->db,$q['RewardFactionId'.$i]);
					$name = $faction->get_name();
					// table QuestXP.dbc hardcoded here (static content with few data)
					switch ($q['RewardFactionValueId'.$i]) {
						case 0: $rep = 0; break;
						case 1: $rep = 10; break;
						case 2: $rep = 25; break;
						case 3: $rep = 75; break;
						case 4: $rep = 150; break;
						case 5: $rep = 250; break;
						case 6: $rep = 350; break;
						case 7: $rep = 500; break;
						case 8: $rep = 1000; break;
						case 9: $rep = 5; break;
					};
					$gain_reputations[] = array('name' => $name, 'value' => $rep);
				}
			}

			// lets lookup name directly - involwing whole new Npc() is not worth of it
			$get_creature_starter = $this->db->query('
				SELECT ct.`entry`,ct.`name`
				FROM `'.$this->db->worlddb.'`.`creature_queststarter` AS cq
				LEFT JOIN `'.$this->db->worlddb.'`.`creature_template` AS ct ON (cq.`id`=ct.entry)
				WHERE cq.`quest`=?',
				array($id)
			);

			$creature_starters = $get_creature_starter->fetchAll(PDO::FETCH_ASSOC);

			$get_creature_ender = $this->db->query('
				SELECT ct.`entry`,ct.`name`
				FROM `'.$this->db->worlddb.'`.`creature_questender` AS cq
				LEFT JOIN `'.$this->db->worlddb.'`.`creature_template` AS ct ON (cq.`id`=ct.entry)
				WHERE cq.`quest`=?',
				array($id)
			);

			$creature_enders = $get_creature_ender->fetchAll(PDO::FETCH_ASSOC);


			// again here - lets lookup name directly - involwing whole new Quest() is not worth of all processing it makes

			// $prequests : array
			//   key "simple" = simple prequests.
			//   Key < 0 - group of all prequests that needs to be done.
			//   Key > 0 - group of all prequests but only one can be done
			// $nextquest : leads to one or more quests, but only one can be choosen
			// $nextquests : leads to one or more quests and all one can be choosen
			// $pregroups : groups of prequests


			// first - get next and prequest by their parents
			$pregroups = $this->_lookup_groups_by_nextquest($q['Id']);
			$nextgroups = $this->_lookup_groups_by_prevquest($q['Id']);
			$prequests = array('groups' => array_keys($pregroups), 'data' => $pregroups);
                        $nextquests = array('groups' => array_keys($nextgroups), 'data' => $nextgroups);

			// then look on the NextQuestId,NextQuestIdChain and PrevQuestId - if there is quest with different id, add it
			if ($q['NextQuestId'] > 0) {
				if ((array_key_exists('simple',$nextquests['data']) && !$this->_search_quest_array($q['NextQuestId'],$nextquests['data']['simple']))  || !array_key_exists('simple',$nextquests['data'])) {
					$nextquests['data']['simple'][] = $this->_lookup_name_by_id($q['NextQuestId']);
					if (!in_array('simple',$nextquests['groups'])) {
						$nextquests['groups'][] = 'simple';
					}
				}
			}
			if ($q['NextQuestIdChain'] > 0) {
				if ((array_key_exists('simple',$nextquests['data']) && !$this->_search_quest_array($q['NextQuestIdChain'],$nextquests['data']['simple']))  || !array_key_exists('simple',$nextquests['data'])) {
					$nextquests['data']['simple'][] = $this->_lookup_name_by_id($q['NextQuestIdChain']);
					if (!in_array('simple',$nextquests['groups'])) {
						$nextquests['groups'][] = 'simple';
					}
				}
			}
			if ($q['PrevQuestId'] > 0) {
				if ((array_key_exists('simple',$prequests['data']) && !$this->_search_quest_array($q['PrevQuestId'],$prequests['data']['simple']))  || !array_key_exists('simple',$prequests['data'])) {
					$prequests['data']['simple'][] = $this->_lookup_name_by_id($q['PrevQuestId']);
					if (!in_array('simple',$prequests['groups'])) {
						$prequests['groups'][] = 'simple';
					}
				}
			}


			$this->_quest = array(
				'Id' => $q['Id'],
				'Title' => $q['Title'],
				'Objectives' => $q['Objectives'],
				'Details' => $q['Details'],
				'Method' => $q['Method'],
				'Level' => $q['Level'],
				'MinLevel' => $q['MinLevel'],
				'RequiredRaces' => $q['RequiredRaces'],
				'ZoneOrSort' => $q['ZoneOrSort'],
				'Type' => $q['Type'],
				'RewardOrRequiredMoney' => $q['RewardOrRequiredMoney'],
				'Experiences' => $xp,
				'RequiredNpcs' => $required_npcs,
				'RequiredObjects' => $required_objects,
				'RequiredItems' => $required_items,
				'RewardItems' => $reward_items,
				'ChooseItems' => $choose_items,
				'GainReputations' => $gain_reputations,
				'CreatureStarters' => $creature_starters,
				'CreatureEnders' => $creature_enders,
				'Prequests' => $prequests,
				'Nextquests' => $nextquests,


			);
			$this->store_cache(array('quest',$id),$this->_quest);
		}
	}

	/**
	 * Returns quest informations
	 * @return array quest informations
	 */
	public function get_quest() {
		if (!$this->_quest['Id'])
			return;

		return $this->_quest;
	}

	/**
	 * Returns quest title
	 * @return string quest title
	 */
	public function get_title() {
		return $this->_quest['Title'];
	}


	private function _lookup_name_by_id($questid,$add = NULL) {
		$get_quest = $this->db->query('
			SELECT `Id`,`Title`,`RequiredRaces`
			FROM `'.$this->db->worlddb.'`.`quest_template`
			WHERE `Id`=?',
			array($questid)
		);

		$quest = $get_quest->fetch(PDO::FETCH_ASSOC);
		if ($add) {
			return array_merge($quest,$add);
		} else {
			return $quest;
		}
	}


	private function _lookup_groups_by_nextquest($questid) {
		$get_quests = $this->db->query('
			SELECT `Id`,`ExclusiveGroup`,`Title`,`RequiredRaces`
			FROM `'.$this->db->worlddb.'`.`quest_template`
			WHERE ? IN (`NextQuestId`,`NextQuestIdChain`)',
			array($questid)
		);

		$groups = array();
		foreach ($get_quests->fetchAll(PDO::FETCH_ASSOC) as $q) {
			if ($q['ExclusiveGroup'] == 0)
				$q['ExclusiveGroup'] = 'simple';
			$groups[$q['ExclusiveGroup']][] = array('Id' => $q['Id'], 'Title' => $q['Title'], 'state' => 'completed', 'RequiredRaces' => $q['RequiredRaces']);
		}
		return $groups;
	}

	private function _lookup_groups_by_prevquest($questid) {
		$get_quests = $this->db->query('
			SELECT `Id`,`ExclusiveGroup`,`Title`,`RequiredRaces`
			FROM `'.$this->db->worlddb.'`.`quest_template`
			WHERE PrevQuestId = ?',
			array($questid)
		);

		$groups = array();
		foreach ($get_quests->fetchAll(PDO::FETCH_ASSOC) as $q) {
			if ($q['ExclusiveGroup'] == 0)
				$q['ExclusiveGroup'] = 'simple';
			$groups[$q['ExclusiveGroup']][] = array('Id' => $q['Id'], 'Title' => $q['Title'], 'RequiredRaces' => $q['RequiredRaces']);
		}
		return $groups;
	}

	private function _search_quest_array($id,$arr) {
		foreach ($arr as $a) {
			if ($a['Id'] == $id) {
				return true;
			}
		}
		return false;

	}

}
