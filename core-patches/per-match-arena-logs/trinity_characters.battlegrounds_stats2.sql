DROP TABLE IF EXISTS `battlegrounds_stats2`;

CREATE TABLE `battlegrounds_stats2` (
        `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `instance_id` BIGINT(10) UNSIGNED NULL DEFAULT NULL,
        `bg_type` CHAR(10) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
        `bg_typeid` TINYINT(3) UNSIGNED NULL DEFAULT NULL,
        `start_time` DATETIME NULL DEFAULT NULL,
        `end_time` DATETIME NULL DEFAULT NULL,
        `winner` TINYINT(1) UNSIGNED NULL DEFAULT NULL COMMENT '0-nobody, 1-alliance, 2-horde',
        UNIQUE INDEX `id` (`id`),
        INDEX `instance_id` (`instance_id`),
        INDEX `bg_typeid` (`bg_typeid`)
) COLLATE='utf8_unicode_ci' ENGINE=MyISAM AUTO_INCREMENT=1;
